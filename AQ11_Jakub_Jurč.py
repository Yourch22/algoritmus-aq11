# -*- coding: utf-8 -*-
"""
Created on Sun May 2 16:58:02 2021

AQ11 algorithm.

@author: Jakub Jurč, KKUI FEI TUKE
"""

#1. Načítanie dát zo súbora.
fvstup = open("data.txt", "r")
riadky = sum(1 for line in fvstup)
riadkye1 = 0
fvstup.seek(0)
stlpce = 5
pole = [[0 for y in range(stlpce)] for z in range(riadky)] 
for x in range(0, riadky):
    for z in range(0, stlpce):
        pole[x][z] = fvstup.read(1)
        if pole[x][z] == '-' :
            riadkye1 += 1
        medzera = fvstup.read(1)
fvstup.close()

#2. Rozdelenie množín.
e1 = [[0 for y in range(stlpce-1)] for z in range(riadkye1)]
e2 = [[0 for y in range(stlpce-1)] for z in range(riadky - riadkye1)]
riadkypomoc = 0
riadkye2 = 0
for a in range(0, riadky):
    if pole[a][4] == '-' :
        e1[riadkypomoc][0] = pole[a][0]
        e1[riadkypomoc][1] = pole[a][1]
        e1[riadkypomoc][2] = pole[a][2]
        e1[riadkypomoc][3] = pole[a][3]
        riadkypomoc += 1
    if pole[a][4] == '+' :
        e2[riadkye2][0] = pole[a][0]
        e2[riadkye2][1] = pole[a][1]
        e2[riadkye2][2] = pole[a][2]
        e2[riadkye2][3] = pole[a][3]
        riadkye2 += 1

#3. Algoritmus
obalky = [[0 for y in range(stlpce-2)] for z in range(riadkye2)] 
pozicka = 0
viacero = []
for e1index in range(0, riadkye1):                                  #kontrola pokrytia obálok
    cont = 0
    #print(e1[e1index][pozicka+1])
    if(e1index > 0):
        for viaceroIndex in range(0, len(viacero)):
            #print(viacero[viaceroIndex])
            if e1[e1index][pozicka+1] != viacero[viaceroIndex]:
                cont = 1
    if(cont == 1):
        continue
    viacero = []
    ostatok = []
    for e2index in range(0, riadkye2):                              #porovnávanie obálok
        for stlpceIndex in range(1, stlpce-1):
            #print(e1[e1index][stlpceIndex], e2[e2index][stlpceIndex])
            if e1[e1index][stlpceIndex] != e2[e2index][stlpceIndex] :
                obalky[e2index][stlpceIndex-1] = e2[e2index][stlpceIndex]
                #print(obalky[e2index][stlpceIndex-1], e2index, stlpceIndex-1)
            if e1[e1index][stlpceIndex] == e2[e2index][stlpceIndex] :
                obalky[e2index][stlpceIndex-1] = 0
                #print(obalky[e2index][stlpceIndex-1], e2index, stlpceIndex-1)
    print(obalky)
    znaky = []                                                      
    for obalkystlpec in range(0, stlpce-2):                         #doladzovanie detailov
        for obalkyriadok in range(0, riadkye2):                     #hľadanie pozície znaku
            if obalky[obalkyriadok][obalkystlpec] != 0 :
                znaky.append(obalky[obalkyriadok][obalkystlpec])
    #print(znaky)
    znak = max(set(znaky), key = znaky.count)
    viacero.append(znak)
    for obalkystlpec in range(0, stlpce-2):
        for obalkyriadok in range(0, riadkye2):
            if obalky[obalkyriadok][obalkystlpec] == znak :
                pozicka = obalkystlpec
                break
    #print(pozicka)
    for obalkyriadok in range(0, riadkye2):
        if (obalky[obalkyriadok][pozicka] != znak) and (obalky[obalkyriadok][pozicka] != 0) and (obalky[obalkyriadok][pozicka] not in viacero):
            viacero.append(obalky[obalkyriadok][pozicka])
    for poleriadok in range(0, riadky):
        if (pole[poleriadok][pozicka+1] not in viacero) and (pole[poleriadok][pozicka+1] not in ostatok):
            ostatok.append(pole[poleriadok][pozicka+1])
    #print(ostatok)
    print('Stĺpec č.', pozicka+1, 'nemôže mať hodnotu/y : ', viacero, 'preto má hodnotu/y : ', ostatok)
    fvystup = open("output.txt", "a")
    fvystup.write("Stĺpec č.")
    fvystup.write(str(pozicka+1))
    fvystup.write(" nemôže mať hodnotu/y : ")
    fvystup.write(str(viacero))
    fvystup.write(" preto má hodnotu/y : ")
    fvystup.write(str(ostatok))
    fvystup.write(" \n")
    fvystup.close()

#print((0 or 'b' or 0) and (0 or 'c' or 0) and ('n' or 'b' or 0))